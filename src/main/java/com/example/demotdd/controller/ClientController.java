package com.example.demotdd.controller;

import com.example.demotdd.model.ClientEntity;
import com.example.demotdd.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clients")
public class ClientController {

    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/")
    public ResponseEntity<List<ClientEntity>> getAllClients() {
        List<ClientEntity> clients = clientService.getAllClients();
        return ResponseEntity.ok(clients);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ClientEntity> getClientById(@PathVariable("id") Long id) {
        ClientEntity client = clientService.getClientById(id);
        if (client == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(client);
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<ClientEntity> getClientByEmail(@PathVariable("email") String email) {
        ClientEntity client = clientService.getClientByEmail(email);
        if (client == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(client);
    }

    @PostMapping("/save")
    public ResponseEntity<String> saveClient(@RequestBody ClientEntity client) {
        clientService.addClient(client);
        return ResponseEntity.status(HttpStatus.CREATED).body("Client saved successfully");
    }

    @PostMapping("/save/all")
    public ResponseEntity<String> saveClients(@RequestBody List<ClientEntity> clients) {
        clientService.addClients(clients);
        return ResponseEntity.status(HttpStatus.CREATED).body("Client saved successfully");
    }

    @PutMapping("/{id}")
    public ResponseEntity<String> editClient(@PathVariable("id") Long id, @RequestBody ClientEntity client) {
        client.setId(id);
        clientService.editClient(client);
        return ResponseEntity.ok("Client updated successfully");
    }

    @PutMapping("/disable/{id}")
    public ResponseEntity<String> disableClient(@PathVariable("id") Long id) {
        clientService.disableClient(id);
        return ResponseEntity.ok("Client disabled successfully");
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteClient(@PathVariable("id") Long id) {
        clientService.deleteClient(id);
        return ResponseEntity.ok("Client deleted successfully");
    }
}
