package com.example.demotdd.dao;

import com.example.demotdd.model.ClientEntity;
import com.example.demotdd.model.Gender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long> {
    ClientEntity findByEmail(String email);
    List<ClientEntity> findByGender(Gender gender);
    List<ClientEntity> findByName (String name);

}
