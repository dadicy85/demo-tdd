package com.example.demotdd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@EntityScan("com.example.demotdd.model")
@SpringBootApplication
public class DemotddApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemotddApplication.class, args);
    }

}
