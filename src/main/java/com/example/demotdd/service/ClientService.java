package com.example.demotdd.service;
import com.example.demotdd.dao.ClientRepository;
import com.example.demotdd.model.ClientEntity;
import com.example.demotdd.model.Gender;
import com.example.demotdd.model.GenderEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    private final ClientRepository clientRepository;

    @Autowired
    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public List<ClientEntity> getAllClients() {
        return clientRepository.findAll();
    }

    public ClientEntity getClientById(Long id) {
        return clientRepository.findById(id).orElse(null);
    }

    public ClientEntity updateClient(Long id, ClientEntity updatedClient) {
        ClientEntity existingClient = clientRepository.findById(id).orElse(null);
        if (existingClient != null) {
            existingClient.setName(updatedClient.getName());
            return clientRepository.save(existingClient);
        }
        return null;
    }

    public ClientEntity getClientByEmail(String email) {
        return clientRepository.findByEmail(email);
    }

    public void addClient(ClientEntity client) {
        clientRepository.save(client);
    }

    public void addClients(List<ClientEntity> clients) {
        clientRepository.saveAll(clients);
    }

    public void editClient(ClientEntity client) {
        clientRepository.save(client);
    }

    public void disableClient(Long id) {
        ClientEntity client = clientRepository.findById(id).orElse(null);
        if (client != null) {
            client.setActive(false);
        }
        clientRepository.save(client);
    }

    public boolean deleteClient(Long id) {
        Optional<ClientEntity> optionalClient = clientRepository.findById(id);
        if (optionalClient.isPresent()) {
            ClientEntity client = optionalClient.get();
            clientRepository.delete(client);
            return true;
        }
        return false;
    }
}
