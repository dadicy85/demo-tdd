package com.example.demotdd.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "gender")
public class GenderEntity {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "male")
    private String male;

    @Column(name = "female")
    private String female;

    @Column(name = "other")
    private String other;

    public GenderEntity(String id, String male, String female, String other) {
        this.id = id;
        this.male = male;
        this.female = female;
        this.other = other;
    }

    // Setter for gender field
    public void setGender(String gender) {
        if (gender.equalsIgnoreCase("male")) {
            this.male = gender;
        } else if (gender.equalsIgnoreCase("female")) {
            this.female = gender;
        } else {
            this.other = gender;
        }
    }
}