package com.example.demotdd.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "client")

public class ClientEntity {
    private Gender gender;
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "birthdate")
    private Date birthdate;

    @Column(name = "is_active")
    private boolean isActive;


    @ManyToOne
    @JoinColumn(name = "gender_id")
    @Enumerated(EnumType.STRING)
    private GenderEntity genderEntity;
    private boolean enabled;

    public ClientEntity(Long id, String email, String firstname, String name, String phone,
                        Date birthdate, boolean isActive, Gender gender) {
        this.id = id;
        this.email = email;
        this.firstname = firstname;
        this.name = name;
        this.phone = phone;
        this.birthdate = birthdate;
        this.isActive = isActive;
        this.gender = gender;
    }
}
