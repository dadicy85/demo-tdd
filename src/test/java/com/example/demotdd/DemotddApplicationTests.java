package com.example.demotdd;

import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;

@EntityScan("com.example.demotdd.model")
@SpringBootTest
class DemotddApplicationTests {

    @Test
    void contextLoads() {
    }

}
