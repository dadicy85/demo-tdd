package com.example.demotdd.service;

import com.example.demotdd.dao.ClientRepository;
import com.example.demotdd.model.ClientEntity;
import com.example.demotdd.model.Gender;
import com.example.demotdd.service.ClientService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

@SpringBootTest
public class ClientEntityServiceTest {
    @Mock
    private ClientRepository clientRepository;

    @InjectMocks
    private ClientService clientService;

    @Test
    public void getAllClients() {
        List<ClientEntity> expectedClients = new ArrayList<>();
        ClientEntity client1 = new ClientEntity();
        client1.setId(123456789L);
        client1.setName("John Doe");
        expectedClients.add(client1);

        ClientEntity client2 = new ClientEntity();
        client1.setId(987654321L);
        client1.setName("Jane Doe");
        expectedClients.add(client2);

        when(clientRepository.findAll()).thenReturn(expectedClients);

        List<ClientEntity> actualClients = clientService.getAllClients();

        assertNotNull(actualClients);
        assertEquals(expectedClients, actualClients);
        verify(clientRepository, times(1)).findAll();
    }
    @Test
    public void findById() {
        Long clientId = 123456789L;
        ClientEntity expectedClient = new ClientEntity();
        expectedClient.setId(clientId);
        expectedClient.setName("John Doe");

        when(clientRepository.findById(clientId)).thenReturn(Optional.of(expectedClient));

        List<ClientEntity> actualClient = clientService.getAllClients();

        assertNotNull(actualClient);
        Assertions.assertEquals(expectedClient, actualClient);
        verify(clientRepository, times(1)).findById(clientId);
    }

    @Test
    void searchByEmail() {
        String email = "john@example.com";
        ClientEntity expectedClient = new ClientEntity();
        expectedClient.setId(123456789L);
        expectedClient.setName("John Doe");
        expectedClient.setEmail(email);

        when(clientRepository.findByEmail(email)).thenReturn(expectedClient);

        ClientEntity actualClient = clientService.getClientByEmail(email);

        assertNotNull(actualClient);
        assertEquals(expectedClient, actualClient);
        verify(clientRepository, times(1)).findByEmail(email);
    }

    @Test
    void addClient() {
        ClientEntity client = new ClientEntity();
        client.setId(123456789L);
        client.setName("John Doe");

        clientService.addClient(client);

        verify(clientRepository, times(1)).save(client);
    }

    @Test
    void addClients() {
        List<ClientEntity> clients = new ArrayList<>();
        ClientEntity client1 = new ClientEntity();
        client1.setId(123456789L);
        client1.setName("John Doe");
        client1.setGender(Gender.MALE);
        clients.add(client1);

        ClientEntity client2 = new ClientEntity();
        client2.setId(987654321L);
        client2.setName("Jane Doe");
        client2.setGender(Gender.FEMALE);
        clients.add(client2);

        clientService.addClients(clients);

        verify(clientRepository, times(1)).saveAll(clients);
    }

    @Test
    void editClient() {
        Long clientId = 123456789L;

        ClientEntity client = new ClientEntity();
        client.setId(clientId);
        client.setName("John Doe");

        when(clientRepository.findById(clientId)).thenReturn(java.util.Optional.of(client));
        clientService.editClient(client);
        verify(clientRepository, times(1)).save(client);
    }

    @Test
    void disableClient() {
        Long clientId = 123456789L;

        ClientEntity client = new ClientEntity();
        client.setId(clientId);
        client.setEnabled(true);

        when(clientRepository.findById(clientId)).thenReturn(java.util.Optional.of(client));
        clientService.disableClient(clientId);
        verify(clientRepository, times(1)).save(client);
    }

    @Test
    void deleteClient() {
        Long clientId = 123456789L;

        ClientEntity client = new ClientEntity();
        client.setId(clientId);
        client.setEnabled(true);
        when(clientRepository.findById(clientId)).thenReturn(java.util.Optional.of(client));
        clientService.deleteClient(clientId);
        verify(clientRepository, times(1)).deleteById(clientId);
    }
}